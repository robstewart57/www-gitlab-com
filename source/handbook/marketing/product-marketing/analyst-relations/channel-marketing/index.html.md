---
layout: markdown_page
title: "Channel Marketing"
---

---

## On this page
{:.no_toc}

- TOC
{:toc}

## Channel marketing at GitLab

- Support resellers to be successful and grow their business
- Motivate resellers to reach first for GitLab
- Promote [Reseller program](/resellers/program/) to recruit new resellers


##### Reseller funds allocation determination

- Resellers will request event, SWAG or campaign support by creating an issue using the [reseller issue template](https://gitlab.com/gitlab-com/resellers/issues/new). When the reseller has completed the issue template detailing their needs, Product Marketing and the Channel Reseller Director will be notified.
- When a reseller requests funds for online marketing campaigns, let them know that we can run the campaign in-house working with the Demand Generation team, and even provide artwork if required. All we need is the redirect links to the: /gitlab.com on their website.
- Post-event or campaign, set up a catch-up call with the reseller to determine ROI of GitLab investment.
- At the post event catch-up with the reseller, there are two tabs in the "GitLab events sponsorship request form (Responses)" sheet found on the Google Drive to be updated - one for events and the other online marketing campaigns.
- Request that the reseller send photos of the event, write a couple of short paragraphs on their experience at the event, any highlights, and impact on their business and GitLab, and send to you. Photos and a short blog post could feature in the Reseller newsletter, the company-wide newsletter.
- After the Online Marketing campaign, we will send the reseller a summary from Google Adwords or equivalent with the metric measurements detailing the success of the campaign. Store campaign summaries in the "Online Marketing Campaign Summaries" folder on the Google Drive.
- As we gather more feedback, we will be able to assign a ROI to our marketing investments, and drive more SQLs.

- View the [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970) to see what's currently in progress.

### Which channel marketing manager should I contact?

- Listed below are areas of responsibility within the channel marketing team:

  - [Tina](/company/team/#t_sturgis), Manager, Partner and Channel Marketing
  - [Ashish](/company/team/#kuthiala), Director PMM
