---
layout: markdown_page
title: "Facebook response workflow"
---

## On this page
{:.no_toc}

- TOC
{:toc}

----

## Overview

## Workflow

## Best practices

## Automation

Messages sent to our [Facebook page](https://www.facebook.com/gitlab/) feed into ZenDesk.
