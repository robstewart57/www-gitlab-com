---
layout: markdown_page
title: "Safeguard"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations/).

## On this page
{:.no_toc}

- TOC
{:toc}

----

The following benefits are provided by [Safeguard](https://www.safeguardglobal.com/) and apply to team members who are contracted through Safeguard. If there are any questions, these should be directed to People Operations at GitLab who will then contact the appropriate individual at Safeguard.

## Ireland

- Currently Safeguard do not provide private healthcare
- Safeguard do provide a pension via Zurich, if individuals would like to join this scheme, a leaflet can be found by clicking on this [link](https://drive.google.com/file/d/1GRasMwjchtKSw4ZkPJNJsiCjAU-MOsNH/view?usp=sharing). Please note that at this time there are no employer contributions.

## Spain

_We are currently unable to hire any more employees or contractors in Spain. Please see [Country Hiring Guidelines](/jobs/faq/#country-hiring-guidelines) for more information._

- Currently Safeguard does not provide private healthcare
- Accruals for 13th and 14th month salaries
- General risks and unemployment insurance
- Salary guarantee fund (FOGASA)
- Work accident insurance 

## Other countries

Safeguard uses multiple third parties across the globe to assist in locations where they do not have a direct payroll.